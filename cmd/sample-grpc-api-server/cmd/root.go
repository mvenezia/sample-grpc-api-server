package cmd

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	versioncmd "gitlab.com/mvenezia/sample-grpc-api-server/cmd/sample-grpc-api-server/cmd/version"
	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver"
	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/configuration"
	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/constants"
)

var (
	rootCmd = &cobra.Command{
		Use:   "sample-grpc-api-server",
		Short: "Sample gRPC API Server",
		Long:  `A sample gRPC API Server`,
		Run: func(cmd *cobra.Command, args []string) {
			_ = runWebServer()
		},
	}
)

func init() {
	// Setup viper
	viper.SetEnvPrefix(constants.ProjectPrefix)
	replacer := strings.NewReplacer("-", "_")
	viper.SetEnvKeyReplacer(replacer)

	// Parse all the CLI Flags
	parseFlags()

	// Bind the flags up
	err := viper.BindPFlags(rootCmd.Flags())
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	viper.AutomaticEnv()

	// Add the known goflags
	rootCmd.Flags().AddGoFlagSet(flag.CommandLine)

	// add sub-commands - namely the version
	rootCmd.AddCommand(versioncmd.NewCommand())
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func parseFlags() {
	rootCmd.Flags().Int(constants.GRPCPort, constants.GRPCPortDefault, constants.GRPCPortDescription)
	rootCmd.Flags().Int(constants.RESTPort, constants.RESTPortDefault, constants.RESTPortDescription)
	rootCmd.Flags().Int(constants.PrometheusPort, constants.PrometheusPortDefault, constants.PrometheusPortDescription)
	rootCmd.Flags().Int(constants.GRPCUIPort, constants.GRPCUIPortDefault, constants.GRPCUIPortDescription)
	rootCmd.Flags().String(constants.OIDCURL, constants.OIDCURLDefault, constants.OIDCURLDescription)
	rootCmd.Flags().String(constants.OIDCAudience, constants.OIDCAudienceDefault, constants.OIDCAudienceDescription)
	rootCmd.Flags().Bool(constants.GRPCAuthentication, constants.GRPCAuthenticationDefault, constants.GRPCAuthenticationDescription)
	rootCmd.Flags().Bool(constants.GRPCAuthorization, constants.GRPCAuthorizationDefault, constants.GRPCAuthorizationDescription)
	rootCmd.Flags().Bool(constants.GRPCPrometheus, constants.GRPCPrometheusDefault, constants.GRPCPrometheusDescription)
	rootCmd.Flags().Bool(constants.GRPCRecovery, constants.GRPCRecoveryDefault, constants.GRPCRecoveryDescription)
	rootCmd.Flags().Int(constants.LogLevel, constants.LogLevelDefault, constants.LogLevelDescription)
	rootCmd.Flags().String(constants.LogOutput, constants.LogOutputDefault, constants.LogOutputDescription)
	rootCmd.Flags().String(constants.LogErrorOutput, constants.LogErrorOutputDefault, constants.LogErrorOutputDescription)
	rootCmd.Flags().String(constants.LogFormat, constants.LogFormatDefault, constants.LogFormatDescription)
	rootCmd.Flags().Bool(constants.MetricsEnabled, constants.MetricsEnabledDefault, constants.MetricsEnabledDescription)
}

func runWebServer() error {
	// Create the server and give it a config
	server := apiserver.New(configuration.Server{
		Authentication: configuration.Authentication{
			OIDCAudience: viper.GetString(constants.OIDCAudience),
			OIDCURL:      viper.GetString(constants.OIDCURL),
		},
		Logging: configuration.Logging{
			Format:          viper.GetString(constants.LogFormat),
			Level:           viper.GetInt(constants.LogLevel),
			OutputPath:      viper.GetString(constants.LogOutput),
			ErrorOutputPath: viper.GetString(constants.LogErrorOutput),
		},
		Services: configuration.Services{
			GRPC: configuration.GRPC{
				Port: viper.GetString(constants.GRPCPort),
				Interceptors: configuration.GRPCInterceptors{
					AuthenticationEnabled: viper.GetBool(constants.GRPCAuthentication),
					AuthorizationEnabled:  viper.GetBool(constants.GRPCAuthorization),
					PrometheusEnabled:     viper.GetBool(constants.GRPCPrometheus),
					RecoveryEnabled:       viper.GetBool(constants.GRPCRecovery),
				},
			},
			GRPCUI: configuration.GRPCUI{
				Port: viper.GetString(constants.GRPCUIPort),
			},
			Prometheus: configuration.Prometheus{
				Port: viper.GetString(constants.PrometheusPort),
			},
			Rest: configuration.Rest{
				Port: viper.GetString(constants.RESTPort),
			},
		},
		Metrics: configuration.Metrics{
			Enabled: viper.GetBool(constants.MetricsEnabled),
		},
	})
	return server.Serve()
}
