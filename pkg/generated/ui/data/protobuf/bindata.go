// Code generated for package protobuf by go-bindata DO NOT EDIT. (@generated)
// sources:
// code/api/sample-grpc-api-spec/api/math/v1/mathv1.proto
// code/api/sample-grpc-api-spec/api/math/v1/proto.lock
// code/api/sample-grpc-api-spec/api/version/v1/proto.lock
// code/api/sample-grpc-api-spec/api/version/v1/versionv1.proto
package protobuf

import (
	"github.com/elazarl/go-bindata-assetfs"
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func bindataRead(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	clErr := gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}
	if clErr != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

type asset struct {
	bytes []byte
	info  os.FileInfo
}

type bindataFileInfo struct {
	name    string
	size    int64
	mode    os.FileMode
	modTime time.Time
}

// Name return file name
func (fi bindataFileInfo) Name() string {
	return fi.name
}

// Size return file size
func (fi bindataFileInfo) Size() int64 {
	return fi.size
}

// Mode return file mode
func (fi bindataFileInfo) Mode() os.FileMode {
	return fi.mode
}

// Mode return file modify time
func (fi bindataFileInfo) ModTime() time.Time {
	return fi.modTime
}

// IsDir return file whether a directory
func (fi bindataFileInfo) IsDir() bool {
	return fi.mode&os.ModeDir != 0
}

// Sys return file is sys mode
func (fi bindataFileInfo) Sys() interface{} {
	return nil
}

var _mathV1Mathv1Proto = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xac\x54\xcd\x6e\xdb\x3c\x10\xbc\xeb\x29\x16\x3a\x25\x07\x8b\xf1\xf7\x15\x3d\xc8\x30\xd0\x5e\xfa\x73\x08\x10\x24\x41\x7b\x0c\x36\xec\x86\x62\x4b\x71\x59\x72\xa5\x34\x2d\xf2\xee\x05\x69\xc9\x50\x7e\x5c\xb4\x40\x7c\xb0\xa4\xdd\xd9\x99\xd1\xac\xc0\x74\xe7\x05\x7f\xc0\x16\xea\x10\x59\xf8\xff\x7a\x53\x55\x01\xf5\x37\x34\x04\xe3\x7a\x53\x71\x10\xcb\x1e\x0c\x5f\xcd\xd5\x2d\xd4\xc6\x8a\xc3\xeb\x46\x73\xaf\xfa\x91\x3c\xfd\xb4\xa8\x12\xf6\xc1\xd1\xca\xc4\xa0\x57\x18\xac\xea\x51\x3a\x35\xae\x33\x9f\xed\x03\x47\x81\xda\x30\x1b\x47\x2a\x77\xd1\x7b\x16\xcc\xd4\xa9\x29\xc2\xf5\x66\x0f\x2b\xcf\x7a\x65\xc8\xaf\xd2\x2d\x1a\x43\x51\xed\x5c\xa4\x67\xc7\xaa\xd9\xe3\x51\xd6\x6e\x0c\x0a\xdd\xe2\xdd\xae\xad\xaf\x0c\xf9\xab\x89\xa5\x99\x58\x1a\x0e\xe4\x31\xd8\xf1\xbf\xb9\x73\x0c\x5b\xf8\x55\x01\x58\x7f\xc3\x6d\xb9\x03\x10\x2b\x8e\x5a\xa8\x2f\xca\x7b\xc1\x29\x4a\x07\x6f\xcf\x3e\xd6\x9b\xd2\x1d\x29\x26\xcb\xbe\x85\x7a\x5c\x37\x27\xcd\xc9\x54\xd6\xec\x05\xb5\xcc\x1c\x00\x1e\xfb\x4c\x72\x6a\x75\x87\xe4\xe0\xd3\x2e\xac\x09\x0d\x30\x44\xd7\x42\xdd\x89\x84\xd4\x2a\x65\xac\x74\xc3\x2e\xd6\xf1\x11\x90\x7a\xb4\x19\x3a\xc7\xfd\xc6\xe4\x42\x86\x4e\x90\xfb\x7c\x29\x7f\x49\x77\xd4\x53\x6a\xe1\xc3\xe5\xe5\xd9\xc5\x93\x4a\x2e\x68\xf6\x69\x28\x95\x1a\x43\x70\x56\x97\x48\xd5\xd7\xc4\xbe\xd0\x85\xc8\x5f\x06\x7d\xa8\x7f\xbf\xa9\xaa\x44\x71\xb4\x7a\x8a\x25\xbf\xac\x52\xf0\xd9\x3a\x07\x91\x64\x88\x7e\xce\xa7\x24\x1a\xfb\x32\x0e\x78\xcd\x83\x00\x06\x0b\x79\x9a\x62\x05\x10\x83\x86\xf7\x24\x17\xdf\x07\x8c\x04\x47\xfb\xdb\xd3\x64\x8e\x27\xaa\xb4\x28\x9f\x53\x70\x77\xc7\x53\xb8\xfb\xad\x97\xaf\xaa\xc1\x60\x9b\x1c\xe4\xbc\xcb\xfc\x33\x24\xd0\x42\xad\x16\xdf\xa3\x4a\x85\xa9\x5e\x84\xf6\xd8\xc7\x39\xb3\x2c\x45\x99\xe5\x90\x1f\x66\x79\x39\x4f\x91\x59\x9e\xf7\xf5\x0e\xb5\x70\xb4\xe8\x8a\xf6\xfe\xe9\x89\xab\x7d\xe7\x25\x4c\xdd\xcc\x64\x0f\x3c\xdd\x57\x55\x4f\x29\xe5\x83\x60\xb9\xad\xc2\x74\xe3\x18\x05\xac\x0f\x83\xc0\x16\xd6\x9b\x67\xc1\xc5\xda\x02\x1e\x29\x0d\xee\x4f\xf8\x5d\xfc\xff\x20\x30\x2f\xe5\xaf\x44\x96\x61\x4e\x67\x80\xbc\x7e\x75\x58\xe3\x61\xc4\x8b\x89\x47\x12\xbf\x03\x00\x00\xff\xff\x8b\xa6\x08\xd1\x58\x05\x00\x00")

func mathV1Mathv1ProtoBytes() ([]byte, error) {
	return bindataRead(
		_mathV1Mathv1Proto,
		"math/v1/mathv1.proto",
	)
}

func mathV1Mathv1Proto() (*asset, error) {
	bytes, err := mathV1Mathv1ProtoBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "math/v1/mathv1.proto", size: 1368, mode: os.FileMode(420), modTime: time.Unix(1630431185, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var _mathV1ProtoLock = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xe4\x57\x4f\x6f\xdb\x3e\x0c\xbd\xe7\x53\x08\x3a\xfd\x7e\x40\x63\x37\xc3\xb0\x43\x4e\xdb\x65\x7f\x0e\x05\x8a\xb5\xd8\x65\x08\x02\x4e\x61\x14\x6d\xb6\xa4\x49\xb2\x8b\xae\xc8\x77\x1f\x2c\xc7\x4e\x6c\xcb\xb1\x8b\x36\x6b\xd1\xf6\x90\x26\x32\xc5\x47\xf2\x89\x8f\xd6\xdd\x84\x10\xba\xc2\xb5\x90\xc2\x09\x25\x2d\x9d\x93\xef\x13\x42\x08\xb9\xf3\x9f\x84\x50\x6d\x94\x53\x1a\xdc\x86\xce\x09\x4d\xc1\x6d\xf2\x59\xe4\xd7\xe8\x59\x65\xb2\xc2\x35\x9d\xd7\x3b\x08\xa1\x29\x5a\x0b\x1c\xf7\xde\xca\xbf\xbb\x83\xef\x84\x50\x09\x29\x16\x4e\x3f\xa1\xbb\xfa\x9d\x81\xc1\x0b\xcb\x6b\xa7\x3b\x9b\xb5\xc0\x64\xd5\xf6\xd3\xf5\xe5\x6d\xc5\x8a\xce\xc9\xec\xac\xfb\xa0\x02\x12\x52\x67\x8e\x06\x0c\xdc\xad\xf6\x06\xeb\x44\x81\xa3\xad\xe7\xdb\xc6\xef\xc5\xc1\xaf\xed\xd9\x3d\x92\xfb\x8a\x3a\xb9\x3d\x69\x7a\x06\x6d\x96\x3c\x5d\x7e\x4a\xb9\x97\x4d\xa0\x52\xee\xe5\x92\xf8\x11\x98\x53\x46\x40\xf2\xe4\x1c\x0a\xe9\xde\xbd\x3d\x5d\x7e\xcf\x81\xc2\xfb\xa6\x58\x7f\x5f\xec\x9d\x52\x8b\x26\x17\x6c\xac\xc8\x5e\x14\x0a\xde\xca\xda\x68\x36\x32\xe7\x4e\x33\x84\xb2\x13\x72\x59\x25\x78\x44\xd2\xbd\xa9\xca\x5c\xd7\x36\xc4\x4c\x69\xad\x9b\xb3\x69\x28\xdc\x46\xc8\xff\x71\xa5\x78\x82\x11\x68\x11\x6d\x9c\xd3\xff\x07\x10\xfc\x06\xe0\xdc\x20\x07\x87\xab\x1e\xa0\x7e\xb0\x06\x20\xc7\x10\xf7\xb5\x59\x0e\x49\xe6\xed\x62\xd0\x22\x2e\x06\x6a\x9c\xcf\x62\x5b\x56\xb5\x67\xdb\x36\xb8\xbe\x08\xac\x76\x2d\xdb\x56\xdb\x76\x6c\x63\xe8\x2e\xb4\x6f\x2c\xe5\xe1\x41\x40\xfa\x69\xef\xd1\x55\xf2\xaa\xa8\x37\x45\x85\x9f\x1f\xfd\xb5\x6c\x0e\xb3\x7f\x64\x82\x90\x00\xf9\x47\x05\x99\xbc\x12\xee\xd7\x75\x79\xff\x05\xf5\xf7\x1c\x30\x22\xd5\xca\xb8\x81\xf9\x52\xdd\x0c\xca\x72\xfb\xe4\x40\x4a\xe5\xc0\x73\xb7\xbb\x29\x8c\x1c\xd7\x95\x2f\xbf\x89\x4d\x39\xca\xa9\xbd\x01\xce\xd1\xc4\xbb\xb3\x30\xe0\x3b\x98\x86\x06\xf6\x0b\x38\x36\x6e\x28\x07\xbc\xe5\xb3\xbd\x8b\x83\xe0\x7a\x4e\x5f\xcf\x74\xe5\x6a\x59\xc1\xb4\x66\x6c\xcd\x3b\x17\x2e\x81\x1f\x11\x53\x69\x9c\xe6\x28\xf1\x8f\x80\xd8\x42\xaa\x13\x9c\x72\xa3\xd9\xf4\xe0\x58\x8c\xad\xd7\xfe\xac\x1b\xcd\xa2\xe2\x0c\xdf\xc0\x6d\x59\x16\xb6\xe4\x28\x97\xbb\xea\x45\xbb\x5c\x22\xa5\x51\x82\x16\xf9\x9b\xea\x49\xbb\x2b\x8e\x77\xc3\x11\xa9\x10\x72\xad\x42\x3d\x3c\xd8\x5d\x03\x6d\xec\x84\x4b\x42\x2f\x1b\xcd\xda\x5e\xf9\x3a\x92\xe2\x1d\x87\x7c\xb8\xfc\x12\x6a\xa6\x8e\xee\x8d\x00\xcf\xd1\x58\xa1\xe4\x20\x7c\x3e\x8b\xce\xa3\xf3\xc7\x42\x65\x4a\x3a\x60\x7d\x62\xf2\x68\x7a\xe5\xff\x8f\x11\xac\x0b\xc1\x36\x80\x09\xf9\x56\x1e\xda\x5e\xa5\xea\xf3\x35\x22\x96\xcc\x84\xe6\x4b\x37\x94\x42\xc8\xed\x3c\x8e\xb9\x70\x9b\xac\xec\xa5\xfc\x74\x51\x61\x0a\x62\x5c\x5c\x55\x43\xbf\xe7\xc5\x96\x22\xae\xe7\x36\xca\x2d\xdb\x60\x8a\x36\xd4\xa2\x75\x16\x9f\xaf\xaf\x2f\xaf\x3a\xb7\x92\x13\x80\x3c\x04\x83\x29\x69\xb3\x21\x10\xd0\x3a\x11\xcc\x8f\x89\xf8\xa7\x55\xf2\x21\x80\xda\xa8\x55\xc6\x1e\x0a\x38\x66\xf4\x4e\x0e\x57\x8a\xcf\xc5\x64\xfb\x37\x00\x00\xff\xff\x30\x3a\xa7\x57\xa0\x13\x00\x00")

func mathV1ProtoLockBytes() ([]byte, error) {
	return bindataRead(
		_mathV1ProtoLock,
		"math/v1/proto.lock",
	)
}

func mathV1ProtoLock() (*asset, error) {
	bytes, err := mathV1ProtoLockBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "math/v1/proto.lock", size: 5024, mode: os.FileMode(420), modTime: time.Unix(1631037101, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var _versionV1ProtoLock = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xbc\x56\x4f\x6f\xdb\x3e\x0c\xbd\xe7\x53\x08\x3a\xf5\x07\x24\x76\xd3\xdf\xfe\x00\x39\x6d\xa7\xad\x87\x02\xc5\x5a\xf4\x32\x04\x86\x2a\x33\x8a\x36\x59\x12\x24\xd9\x5d\x57\xf4\xbb\x0f\x76\xec\x34\x8e\x25\xdb\x69\x93\xe6\xe0\x24\x32\xc5\xf7\x48\x91\x8f\x7a\x9a\x20\x84\x53\x58\x71\xc9\x1d\x57\xd2\xe2\x05\xfa\x39\x41\x08\xa1\xa7\xea\x89\x10\xd6\x46\x39\xa5\x89\x5b\xe3\x05\xc2\x05\x18\xcb\x95\x2c\xe6\x51\xb5\x8c\xa7\x8d\x55\x0a\x2b\xbc\xd8\x6e\x42\x08\x67\x60\x2d\x61\xf0\xe2\x70\xf3\x79\xda\xf9\x8d\x10\x96\x24\x83\xd2\xef\x37\x70\x97\x72\xa5\x4c\x46\x4a\x16\x57\x96\xe1\x1d\xbb\xe7\xe9\xa1\x0e\x7e\x80\x16\x8f\x78\xda\x36\x5d\x71\x10\xe9\x3e\x9f\xae\xcb\xca\x96\xa7\x78\x81\xe6\xd3\xee\x8b\x06\x8f\x71\x97\xd4\xc9\xc0\x1e\x33\xf7\xa8\x2b\x33\xeb\x0c\x97\xad\x60\x3a\x01\xf5\x31\xb8\x18\x60\x40\x55\x96\x71\x77\x42\x02\xff\x0f\x10\x70\x06\x20\xb1\x8e\x38\x38\x21\x89\x0f\x3d\x24\xee\x73\x2e\xd2\x24\x3d\x2d\x81\x8f\x7d\x59\x50\xef\x50\x07\x9f\x7a\x08\x50\x95\x69\x2e\xc0\x9c\x10\xfe\x73\x0f\xbc\x16\xc4\x95\x7d\xf7\x0a\xf8\xd6\xff\xe5\xc4\xf7\x66\xf9\xe2\x15\x5b\x30\x05\xa7\x63\x15\xe5\xce\x7b\x28\xd8\x68\x3a\x52\x02\xfc\xca\xe2\x0b\x93\xcb\xa4\x89\xb4\xab\x63\x1e\x7b\x95\xbb\xc0\x06\x9f\x6e\x6d\xb6\xe8\xb6\x3a\x0f\xb1\x6f\x45\x70\xc6\x94\x62\x02\x22\xa2\x79\xb4\x76\x4e\xff\xe7\x41\xa8\x36\x10\xc6\x0c\x30\xe2\x20\x0d\x00\x85\xc1\x5a\x80\x0c\x7c\x9a\xb4\x35\x2b\x88\xc8\x2b\xbb\x98\x68\x1e\xd7\xdd\x13\x17\xf3\x98\xef\xe4\x39\xb0\xfd\xd9\xbb\xbe\xf4\xac\x76\x2d\xf7\xad\x0e\x2d\x40\x9e\x69\x65\xdc\x40\xfd\x35\x93\x72\x93\xf3\x2a\x42\x22\xa5\x72\x55\x54\xb6\x1e\x9b\x23\x87\x5b\xe3\xab\xda\x44\x67\x0c\xe4\xcc\x3e\x10\xc6\xc0\xc4\x75\x41\x0c\xf8\xf6\x86\xa1\x09\xfd\x4d\x18\xb4\xc6\xf5\xce\xe1\x15\xf3\x17\x17\x3b\xe4\x02\x25\x18\xe8\x3e\xa6\x92\x06\x66\xaf\x01\xb7\x87\xcf\xb8\x13\xe4\x3e\xa2\x2a\x8b\xb3\x02\x24\xfc\xe5\x24\xb6\x24\xd3\x02\x66\xcc\x68\x3a\x6b\xd7\xc6\xa1\xf7\x81\xb3\xd2\x47\x54\xd6\xf2\x03\x79\xdc\x64\x86\x26\x0c\x64\x52\x27\x30\xaa\xc3\x89\x94\x06\x49\x34\x2f\x2e\x9a\x37\xfb\xdd\xd1\xdf\x15\x3d\xc2\x51\x56\xb3\xaf\x97\x07\xbb\x6c\xa0\x9d\x1d\x77\xc2\x37\xed\xda\xe9\xbd\xa9\x52\x89\x6a\x19\x44\x5f\xaf\x2f\x7d\x2d\xd5\x19\x01\x23\xf0\xc3\xe3\xae\xcd\xa0\x98\x47\xe7\xd1\xf9\xb1\x50\xa9\x92\x8e\xd0\x90\xae\x1c\x4d\xba\xaa\xef\x31\xda\x75\xc5\xe9\x9a\x80\x40\x77\x9b\xd2\x0d\xea\x55\xc8\xd7\x08\x2e\xb9\x11\xa3\xa8\x94\x9a\x6e\x17\x71\xcc\xb8\x5b\xe7\x9b\x8e\x2a\x4e\xc7\x0a\x32\xc2\xc7\xf1\x6a\xda\xfa\x0b\x2b\xb7\x94\xbc\xde\x45\xd5\x47\xdc\x6a\xb6\xb1\xfc\x71\x60\x24\x11\x49\xaa\xa8\x3d\x45\xaf\x86\x8f\xd0\x7b\x7c\x1d\x41\xac\x7b\x6d\x56\x69\xc9\x91\x3a\x29\x05\x4b\x0d\xd7\x81\xdb\x4c\x9b\xdb\x95\x32\x80\xc8\xbd\xca\x1d\x72\x6b\x40\x35\x1d\x54\xd2\x09\x4a\xca\x51\x4f\xc8\xd2\x35\x64\xe0\x3d\x9b\x2d\xc9\xef\xb7\xb7\xd7\x37\xaf\xb9\xdc\x1e\x0a\xf2\x16\x0c\xaa\xa4\xcd\x87\x40\x88\xd6\x82\xd3\x6a\x9c\xc7\xbf\x6c\xf7\x16\x74\x08\xa0\x36\x2a\xcd\xe9\x5b\x01\xc7\x5c\x91\x26\xbb\x2b\xe5\x73\x39\x79\xfe\x17\x00\x00\xff\xff\xb8\x69\xfb\x09\x58\x10\x00\x00")

func versionV1ProtoLockBytes() ([]byte, error) {
	return bindataRead(
		_versionV1ProtoLock,
		"version/v1/proto.lock",
	)
}

func versionV1ProtoLock() (*asset, error) {
	bytes, err := versionV1ProtoLockBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "version/v1/proto.lock", size: 4184, mode: os.FileMode(420), modTime: time.Unix(1631037134, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var _versionV1Versionv1Proto = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x74\x54\x4d\x6f\xe3\x38\x0c\xbd\xfb\x57\x10\x3e\xb5\x87\xd8\x4d\xbb\x1f\x40\x8c\x00\xbb\xd8\x05\x76\x7b\x28\x50\x6c\x8b\xf6\x18\xa8\x0a\x23\x6b\x47\x16\x05\x89\x76\x9a\x29\xf2\xdf\x07\x92\xe5\x7c\x75\x9a\x43\x12\x91\x8f\x4f\x8f\x8f\xb4\xc3\xce\xb2\x78\x87\x25\x94\xce\x13\xd3\x5d\xd9\x14\x85\x13\xf2\x9b\x50\x08\xc3\xbc\x29\xc8\xb1\x26\x0b\x8a\x56\x53\x74\x09\xa5\xd2\x6c\xc4\x5b\x25\xa9\xab\xbb\x01\x2d\x7e\xd7\xa2\x0e\xa2\x73\x06\x67\xca\x3b\x39\x13\x4e\xd7\x03\xfa\xa0\xc9\xd6\xc3\x3c\x52\xea\xce\x91\x67\x28\x15\x91\x32\x58\x47\x80\xb0\x96\x58\x44\xf6\x50\xa5\xbb\xcb\xe6\x00\x4b\x67\x39\x53\x68\x67\x61\x2b\x94\x42\x5f\x8f\x42\xc2\x4f\xcb\x8a\x49\xe6\x55\xbc\xbe\x52\x82\x71\x2b\x76\x63\x5a\xae\x14\xda\x55\x66\xa9\x32\x4b\x45\x0e\xad\x70\x7a\xb8\x9d\x32\xd7\xb0\x84\x8f\x02\x40\xdb\x0d\x2d\xd2\x3f\x00\xd6\x6c\x70\x01\xe5\x53\x6a\x0d\x5e\xc6\x8e\xe0\xcf\xc7\xfb\xb2\x49\x80\xdc\xe3\x02\xca\x61\x5e\xdd\x54\x37\x39\x2c\xc9\xb2\x90\x3c\xd1\x00\x58\xd1\x45\x9e\x07\x2d\x5b\x81\x06\x5e\x46\xcb\x32\x1a\xa0\xf7\x66\x01\x65\xcb\xec\xc2\xa2\xae\x95\xe6\xb6\x1f\xcd\x1d\x2e\x80\xd8\x09\x1d\xa1\x93\xe9\x7f\xa8\x18\x88\xd0\x0c\xd9\xc7\x9f\xf4\x85\xef\x8c\xde\x0a\xb3\x5a\x93\x0c\x93\x92\x4f\x17\x7d\x9a\x62\xee\x68\x16\x6d\xc8\x9c\x6b\x0c\xd2\xeb\xe4\x5b\xec\x81\x3c\x82\x78\xa3\x9e\x81\x5b\x9c\x1c\x48\xb6\x1d\x8c\xd9\x17\x00\x41\xb6\xd8\x61\x58\xc0\xbf\xcf\xcf\x8f\x4f\xcd\x65\x24\x06\x24\xd9\xd0\xa7\x48\x29\x9c\x33\x5a\xa6\xa9\xd6\xff\x07\xb2\x89\xc6\x79\x5a\xf7\xf2\xab\xfc\xbe\x29\x8a\x80\x7e\xd0\xf2\x38\x99\xd8\x65\x5d\xc3\xab\x36\x06\x3c\x72\xef\xed\x99\x40\xdf\x25\x86\x2c\x5f\x38\x0d\x91\x00\x7d\x01\xe0\x9d\x84\x7f\x90\xef\x4f\x50\x57\xe7\xe7\x87\xa0\xae\x33\x69\xb8\xcc\xfd\x87\xce\xec\xae\xb3\xc9\x87\x55\x4c\xab\x5e\x09\xa7\xab\xe8\xf8\xb4\x60\xf1\xa3\x90\x61\x01\x65\x7d\xfe\x9c\xd4\x27\x1a\xcb\x93\x71\x16\xfb\xa2\xe8\x30\x84\xf8\xf0\x7d\xd2\x04\x1f\x5f\xa7\x93\xac\xc9\x94\xe7\x16\x81\x85\x02\xb2\x69\x70\x4a\x33\x78\x74\x14\x34\x93\xdf\xc5\xf1\xb0\xd7\x56\xc5\xf8\x6a\xf2\x6c\x09\xf3\xe6\x58\xdc\x8a\xd0\x02\x6d\x0e\xd5\x92\xba\x4e\xf3\x79\xe5\x18\x83\x25\xdc\xe6\xc2\xd7\x16\xb9\x45\x0f\xe4\xc1\xd2\xb8\x32\xec\x11\x61\x2b\x02\x48\x83\xc2\xc2\xb6\x45\x0b\x6f\xbd\x36\x17\x54\x11\xb6\x0a\x2c\x38\xbe\x71\xee\x32\xdd\xdf\xf1\x48\x9b\x84\x5f\x1f\xf1\xe9\xb8\x5a\x8f\xd8\x5f\x32\x76\x5a\x0a\xda\x80\x22\xe8\x03\xae\x81\x29\xaa\x76\xda\xe0\xc9\x5d\x74\xd2\xef\xaf\xb9\xf6\xaf\x11\xe5\x53\xd9\x11\x2b\xa7\xf0\x12\x7e\xcb\xc8\x47\x23\x38\x1a\x0e\x9a\xc7\xa6\x46\xc8\x1a\x36\xe4\xa1\x06\xdf\x5b\x1b\x2b\xc9\x1e\x59\xdc\x54\xb2\x84\xdf\x9b\x38\xbd\x1f\x01\x00\x00\xff\xff\x46\x1f\x54\x2e\x84\x05\x00\x00")

func versionV1Versionv1ProtoBytes() ([]byte, error) {
	return bindataRead(
		_versionV1Versionv1Proto,
		"version/v1/versionv1.proto",
	)
}

func versionV1Versionv1Proto() (*asset, error) {
	bytes, err := versionV1Versionv1ProtoBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "version/v1/versionv1.proto", size: 1412, mode: os.FileMode(420), modTime: time.Unix(1631037134, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("Asset %s can't read by error: %v", name, err)
		}
		return a.bytes, nil
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// MustAsset is like Asset but panics when Asset would return an error.
// It simplifies safe initialization of global variables.
func MustAsset(name string) []byte {
	a, err := Asset(name)
	if err != nil {
		panic("asset: Asset(" + name + "): " + err.Error())
	}

	return a
}

// AssetInfo loads and returns the asset info for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func AssetInfo(name string) (os.FileInfo, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("AssetInfo %s can't read by error: %v", name, err)
		}
		return a.info, nil
	}
	return nil, fmt.Errorf("AssetInfo %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() (*asset, error){
	"math/v1/mathv1.proto":       mathV1Mathv1Proto,
	"math/v1/proto.lock":         mathV1ProtoLock,
	"version/v1/proto.lock":      versionV1ProtoLock,
	"version/v1/versionv1.proto": versionV1Versionv1Proto,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for childName := range node.Children {
		rv = append(rv, childName)
	}
	return rv, nil
}

type bintree struct {
	Func     func() (*asset, error)
	Children map[string]*bintree
}

var _bintree = &bintree{nil, map[string]*bintree{
	"math": &bintree{nil, map[string]*bintree{
		"v1": &bintree{nil, map[string]*bintree{
			"mathv1.proto": &bintree{mathV1Mathv1Proto, map[string]*bintree{}},
			"proto.lock":   &bintree{mathV1ProtoLock, map[string]*bintree{}},
		}},
	}},
	"version": &bintree{nil, map[string]*bintree{
		"v1": &bintree{nil, map[string]*bintree{
			"proto.lock":      &bintree{versionV1ProtoLock, map[string]*bintree{}},
			"versionv1.proto": &bintree{versionV1Versionv1Proto, map[string]*bintree{}},
		}},
	}},
}}

// RestoreAsset restores an asset under the given directory
func RestoreAsset(dir, name string) error {
	data, err := Asset(name)
	if err != nil {
		return err
	}
	info, err := AssetInfo(name)
	if err != nil {
		return err
	}
	err = os.MkdirAll(_filePath(dir, filepath.Dir(name)), os.FileMode(0755))
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(_filePath(dir, name), data, info.Mode())
	if err != nil {
		return err
	}
	err = os.Chtimes(_filePath(dir, name), info.ModTime(), info.ModTime())
	if err != nil {
		return err
	}
	return nil
}

// RestoreAssets restores an asset under the given directory recursively
func RestoreAssets(dir, name string) error {
	children, err := AssetDir(name)
	// File
	if err != nil {
		return RestoreAsset(dir, name)
	}
	// Dir
	for _, child := range children {
		err = RestoreAssets(dir, filepath.Join(name, child))
		if err != nil {
			return err
		}
	}
	return nil
}

func _filePath(dir, name string) string {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	return filepath.Join(append([]string{dir}, strings.Split(cannonicalName, "/")...)...)
}

func assetFS() *assetfs.AssetFS {
	assetInfo := func(path string) (os.FileInfo, error) {
		return os.Stat(path)
	}
	for k := range _bintree.Children {
		return &assetfs.AssetFS{Asset: Asset, AssetDir: AssetDir, AssetInfo: assetInfo, Prefix: k}
	}
	panic("unreachable")
}
