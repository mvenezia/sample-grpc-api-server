package marshaller

import (
	"fmt"
	"io"
	"io/ioutil"
	"reflect"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
)

type RawJSONPb struct {
	*runtime.JSONPb
}

var typeOfBytes = reflect.TypeOf([]byte(nil))

func (*RawJSONPb) NewDecoder(r io.Reader) runtime.Decoder {
	return runtime.DecoderFunc(func(v interface{}) error {
		rawData, err := ioutil.ReadAll(r)
		if err != nil {
			return err
		}
		rv := reflect.ValueOf(v)

		if rv.Kind() != reflect.Ptr {
			return fmt.Errorf("%T is not a pointer", v)
		}

		rv = rv.Elem()
		if rv.Type() != typeOfBytes {
			return fmt.Errorf("type must be []byte but got %T", v)
		}

		rv.Set(reflect.ValueOf(rawData))
		return nil
	})
}
