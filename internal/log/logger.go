package log

import (
	"fmt"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/configuration"
)

const (
	FormatConsole    = "console"
	FormatJSON       = "json"
	OutputPathStdErr = "stderr"
	OutputPathStdOut = "stdout"

	InvalidLevelError = "invalid log level set"

	invalidOutputPathError    = "invalid output path - acceptable values are "
	invalidErrOutputPathError = "invalid error output path - acceptable values are "
	invalidFormatPathError    = "invalid format - acceptable values are "
)

var (
	logger *zap.Logger

	ErrInvalidLevel           = fmt.Errorf(InvalidLevelError)
	ErrInvalidOutputPath      = fmt.Errorf(invalidOutputPathError + OutputPathStdOut + ", " + OutputPathStdErr)
	ErrInvalidErrorOutputPath = fmt.Errorf(invalidErrOutputPathError + OutputPathStdOut + ", " + OutputPathStdErr)
	ErrInvalidFormat          = fmt.Errorf(invalidFormatPathError + FormatJSON + ", " + FormatConsole)
)

func GetLogger() *zap.Logger {
	return logger
}

func InitializeLogger(inputConfig configuration.Logging) (*zap.Logger, error) {
	// Validating inputs...
	// Atomic Level first...
	atomicLevel := zapcore.Level(inputConfig.Level)
	switch atomicLevel {
	case zap.DebugLevel, zap.InfoLevel, zap.WarnLevel, zap.ErrorLevel, zap.DPanicLevel, zap.PanicLevel, zap.FatalLevel:
		break
	default:
		return nil, ErrInvalidLevel
	}

	// Validating output path
	if inputConfig.OutputPath != OutputPathStdOut && inputConfig.OutputPath != OutputPathStdErr {
		return nil, ErrInvalidOutputPath
	}

	// Validating error output path
	if inputConfig.ErrorOutputPath != OutputPathStdOut && inputConfig.ErrorOutputPath != OutputPathStdErr {
		return nil, ErrInvalidErrorOutputPath
	}

	// Validating error output path
	if inputConfig.Format != FormatJSON && inputConfig.Format != FormatConsole {
		return nil, ErrInvalidFormat
	}

	config := zap.NewProductionConfig()
	config.OutputPaths = []string{inputConfig.OutputPath}
	config.ErrorOutputPaths = []string{inputConfig.ErrorOutputPath}
	config.Level = zap.NewAtomicLevelAt(atomicLevel)
	config.Encoding = inputConfig.Format
	logger, _ = config.Build()

	return logger, nil
}
