package apiserver

import (
	"go.uber.org/zap"

	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/configuration"
)

type Server interface {
	Serve() error
}

type server struct {
	configuration configuration.Server
	logger        *zap.Logger
}

func New(config configuration.Server) Server {
	return &server{
		configuration: config,
	}
}
