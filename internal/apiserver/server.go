package apiserver

import (
	"context"

	"google.golang.org/grpc"

	internalgrpc "gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/grpc"
	mathv1 "gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/grpc/apis/math/v1"
	versionv1 "gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/grpc/apis/version/v1"
	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/grpcui"
	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/prometheus"
	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/rest"
	log2 "gitlab.com/mvenezia/sample-grpc-api-server/internal/log"
)

const (
	LogCreatingVersionv1Server = "creating version/v1 server"
)

func (s *server) Serve() error {
	var err error

	// Prepare everything
	err = s.prepareServer()
	if err != nil {
		return err
	}

	// Any service can create an error and that will cause a shutdown of the program
	errChannel := make(chan error, 1)

	// Spin up grpc
	go func() {
		internalgrpc.New(internalgrpc.ServerConfiguration{
			Port: s.configuration.Services.GRPC.Port,
			Interceptors: internalgrpc.InterceptorConfiguration{
				AuthenticationEnabled: s.configuration.Services.GRPC.Interceptors.AuthenticationEnabled,
				AuthorizationEnabled:  s.configuration.Services.GRPC.Interceptors.AuthorizationEnabled,
				PrometheusEnabled:     s.configuration.Services.GRPC.Interceptors.PrometheusEnabled,
				RecoveryEnabled:       s.configuration.Services.GRPC.Interceptors.RecoveryEnabled,
			},
			Authentication: internalgrpc.AuthenticationConfiguration{
				OIDCAudience: s.configuration.Authentication.OIDCAudience,
				OIDCURL:      s.configuration.Authentication.OIDCURL,
			},
			Authorizations: []internalgrpc.AuthorizationConfiguration{&internalgrpc.VersionV1AuthorizationConfiguration{}},
			APIs:           []internalgrpc.APIConfiguration{&versionv1.API{}, &mathv1.API{}},
			ErrorChannel:   errChannel,
			Logger:         s.logger.Named(internalgrpc.LoggingPackageName),
		}).Serve()
	}()

	// Spin up grpcui
	go func() {
		grpcui.New(grpcui.ServerConfiguration{
			GRPCConnectPort:   s.configuration.Services.GRPC.Port,
			WebListenPort:     s.configuration.Services.GRPCUI.Port,
			Context:           context.Background(),
			ErrorChannel:      errChannel,
			GRPCDialerOptions: []grpc.DialOption{grpc.WithInsecure(), grpc.WithBlock()},
			Logger:            s.logger.Named(grpcui.LoggingPackageName),
		}).Serve()
	}()

	// Spin up prometheus exporter
	go func() {
		prometheus.New(prometheus.ServerConfiguration{
			WebListenPort: s.configuration.Services.Prometheus.Port,
			ErrorChannel:  errChannel,
			Logger:        s.logger.Named(prometheus.LoggingPackageName),
		}).Serve()
	}()

	// Spin up rest webserver
	go func() {
		rest.New(rest.ServerConfiguration{
			ErrorChannel:      errChannel,
			GRPCDialerOptions: []grpc.DialOption{grpc.WithInsecure()},
			GRPCListenAddress: ":" + s.configuration.Services.GRPC.Port,
			RESTListenAddress: ":" + s.configuration.Services.Rest.Port,
			Logger:            s.logger.Named(rest.LoggingPackageName),
		}).Serve()
	}()

	err = <-errChannel

	return err
}

func (s *server) prepareServer() error {
	var err error

	// To begin with, let's set up logging...
	s.logger, err = log2.InitializeLogger(s.configuration.Logging)
	if err != nil {
		return &Error{Message: CannotSetupLoggingError, Err: err}
	}

	s.logger.Info(LogCreatingVersionv1Server)
	versionv1server := versionv1.NewServer(&versionv1.ServerConfig{
		InputLog: s.logger.Named(versionv1.LoggingPackageName),
	})
	versionv1.APIServer = versionv1server
	mathv1server := mathv1.NewServer(&mathv1.ServerConfig{
		InputLog: s.logger.Named(mathv1.LoggingPackageName),
	})
	mathv1.APIServer = mathv1server

	return nil
}
