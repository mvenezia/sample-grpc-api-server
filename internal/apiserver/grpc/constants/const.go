package constants

const (
	AuthorizationHeader    = "authorization"
	AuthorizedUserHeader   = "sample-grpc-api-server.user"
	AuthorizedGroupsHeader = "sample-grpc-api-server.groups"
)
