package grpc

import (
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

const (
	LoggingPackageName = "internal.apiserver.grpc"
)

type Server interface {
	Serve()
}

type Error struct {
	Message string
	Err     error
}

func (e *Error) Error() string { return e.Message + ": " + e.Err.Error() }

type ServerConfiguration struct {
	Port           string
	Interceptors   InterceptorConfiguration
	Authentication AuthenticationConfiguration
	Authorizations []AuthorizationConfiguration
	APIs           []APIConfiguration

	ErrorChannel chan<- error
	Logger       *zap.Logger
}

type InterceptorConfiguration struct {
	AuthenticationEnabled bool
	AuthorizationEnabled  bool
	PrometheusEnabled     bool
	RecoveryEnabled       bool
}

type AuthenticationConfiguration struct {
	OIDCAudience string
	OIDCURL      string
}

type AuthorizationConfiguration interface {
	RegisterAuthorization(logger *zap.Logger) ([]grpc.UnaryServerInterceptor, []grpc.StreamServerInterceptor)
}

type APIConfiguration interface {
	RegisterAPI(*grpc.Server)
}
