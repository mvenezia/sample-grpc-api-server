package grpc

import (
	"net"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/grpc/interceptors/authentication"
	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/grpc/interceptors/logging"
	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/grpc/interceptors/prometheus"
	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/grpc/interceptors/recovery"
	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/grpc/interceptors/tags"
)

const (
	disableGRPCAuthentication = "disabling grpc authentication"
	disableGRPCAuthorization  = "disabling grpc authorization"
	disableGRPCPrometheus     = "disabling grpc prometheus"
	disableGRPCRecovery       = "disabling grpc recovery"
	enableGRPCAuthentication  = "enabling grpc authentication"
	enableGRPCAuthorization   = "enabling grpc authorization"
	enableGRPCPrometheus      = "enable grpc prometheus"
	enableGRPCRecovery        = "enabling grpc recovery"
	initializePrometheus      = "initializing grpc prometheus for grpc server"

	ErrCouldNotListen = "could not start a listener"

	LogStartingServerListAddress = "listenAddress"
	LogStartingServerMessage     = "starting server"
)

type server struct {
	port           string
	interceptors   InterceptorConfiguration
	authentication AuthenticationConfiguration
	authorizations []AuthorizationConfiguration
	apis           []APIConfiguration

	chainedUnaryInterceptors  []grpc.UnaryServerInterceptor
	chainedStreamInterceptors []grpc.StreamServerInterceptor
	grpcServer                *grpc.Server

	errorChannel chan<- error
	logger       *zap.Logger
}

func (s *server) Serve() {
	// Prepare the server
	s.addInterceptors()
	opts := []grpc.ServerOption{
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(s.chainedUnaryInterceptors...)),
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(s.chainedStreamInterceptors...)),
	}
	s.grpcServer = grpc.NewServer(opts...)

	// Register APIs...
	s.registerAPIs()

	// Finalize server
	s.finalizeServer()

	// Start the listener
	grpcListener, err := net.Listen("tcp", ":"+s.port)
	if err != nil {
		s.logger.Error(ErrCouldNotListen, zap.Error(err))
		s.errorChannel <- err
	}

	// Handle Requests
	s.logger.Info(LogStartingServerMessage, zap.String(LogStartingServerListAddress, s.port))
	s.errorChannel <- s.grpcServer.Serve(grpcListener)
}

func (s *server) addInterceptors() {
	// Order matters...
	s.addTagsInterceptors()
	s.addLoggingInterceptors()
	s.addPanicRecoveryInterceptors()
	s.addPrometheusInterceptors()
	s.addAuthenticationInterceptors()
	s.addAuthorizationInterceptors()
}

func (s *server) registerAPIs() {
	for _, api := range s.apis {
		api.RegisterAPI(s.grpcServer)
	}
}

func (s *server) addTagsInterceptors() {
	s.chainedUnaryInterceptors = append(s.chainedUnaryInterceptors, tags.UnaryFunction())
	s.chainedStreamInterceptors = append(s.chainedStreamInterceptors, tags.StreamFunction())
}

func (s *server) addLoggingInterceptors() {
	logging.SetupLogging()
	s.chainedUnaryInterceptors = append(s.chainedUnaryInterceptors, logging.UnaryFunction())
	s.chainedStreamInterceptors = append(s.chainedStreamInterceptors, logging.StreamFunction())
}

func (s *server) addPanicRecoveryInterceptors() {
	if s.interceptors.RecoveryEnabled {
		s.logger.Info(enableGRPCRecovery)
		s.chainedUnaryInterceptors = append(s.chainedUnaryInterceptors, recovery.UnaryFunction())
		s.chainedStreamInterceptors = append(s.chainedStreamInterceptors, recovery.StreamFunction())
	} else {
		s.logger.Info(disableGRPCRecovery)
	}
}

func (s *server) addAuthenticationInterceptors() {
	if s.interceptors.AuthenticationEnabled {
		s.logger.Info(enableGRPCAuthentication)
		authentication.SetupAuthentication(s.logger, s.authentication.OIDCURL, s.authentication.OIDCAudience)
		s.chainedUnaryInterceptors = append(s.chainedUnaryInterceptors, authentication.UnaryAuthenticationInterceptor)
	} else {
		s.logger.Info(disableGRPCAuthentication)
	}
}

func (s *server) addAuthorizationInterceptors() {
	if s.interceptors.AuthorizationEnabled {
		s.logger.Info(enableGRPCAuthorization)
		for _, authPlugin := range s.authorizations {
			newUnaryInterceptors, newStreamInterceptors := authPlugin.RegisterAuthorization(s.logger)
			s.chainedUnaryInterceptors = append(s.chainedUnaryInterceptors, newUnaryInterceptors...)
			s.chainedStreamInterceptors = append(s.chainedStreamInterceptors, newStreamInterceptors...)
		}
	} else {
		s.logger.Info(disableGRPCAuthorization)
	}
}

func (s *server) addPrometheusInterceptors() {
	if s.interceptors.PrometheusEnabled {
		s.logger.Info(enableGRPCPrometheus)
		s.chainedUnaryInterceptors = append(s.chainedUnaryInterceptors, prometheus.UnaryFunction())
		s.chainedStreamInterceptors = append(s.chainedStreamInterceptors, prometheus.StreamFunction())
	} else {
		s.logger.Info(disableGRPCPrometheus)
	}
}

func (s *server) enableGRPCReflection() {
	reflection.Register(s.grpcServer)
}

func (s *server) finalizeServer() {
	s.enableGRPCReflection()

	// If Prometheus is enabled we need to also initialize the metric
	s.initalizePrometheusMetrics()
}

func (s *server) initalizePrometheusMetrics() {
	if s.interceptors.PrometheusEnabled {
		s.logger.Info(initializePrometheus)
		prometheus.RegisterGRPCServer(s.grpcServer)
	}
}

func New(config ServerConfiguration) Server {
	return &server{
		port:           config.Port,
		interceptors:   config.Interceptors,
		authentication: config.Authentication,
		authorizations: config.Authorizations,
		apis:           config.APIs,
		errorChannel:   config.ErrorChannel,
		logger:         config.Logger,
	}
}
