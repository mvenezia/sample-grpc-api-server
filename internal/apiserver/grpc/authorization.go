package grpc

import (
	"go.uber.org/zap"
	"google.golang.org/grpc"

	versionv1api "gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/grpc/interceptors/authorization/version/v1"
)

type VersionV1AuthorizationConfiguration struct{}

func (config *VersionV1AuthorizationConfiguration) RegisterAuthorization(inputLogger *zap.Logger) (unary []grpc.UnaryServerInterceptor, stream []grpc.StreamServerInterceptor) {
	versionv1api.SetupInterceptor(inputLogger)
	unary = append(unary, versionv1api.UnaryInterceptor)
	return
}
