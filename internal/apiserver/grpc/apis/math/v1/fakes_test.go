package v1_test

import (
	"fmt"
	"math"
)

const negativeNumberError = "you gave me a negative number"

type fakemath struct{}

func (fakemath) Square(input float64) float64 {
	return input * input
}

func (fakemath) SquareRoot(input float64) (float64, error) {
	if input < 0 {
		return 0, fmt.Errorf(negativeNumberError)
	}
	return math.Sqrt(input), nil
}

func (fakemath) Factorial(input int64) (int64, error) {
	if input < 0 {
		return 0, fmt.Errorf(negativeNumberError)
	}
	if input < 2 {
		return 1, nil
	}
	output := input
	for x := input - 1; x > 1; x-- {
		output = output * x
	}
	return output, nil
}
