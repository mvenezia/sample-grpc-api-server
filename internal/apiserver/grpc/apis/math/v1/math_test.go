package v1_test

import (
	"context"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/ginkgo/extensions/table"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	v1 "gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/grpc/apis/math/v1"
	"gitlab.com/mvenezia/sample-grpc-api-server/internal/log"
	pb "gitlab.com/mvenezia/sample-grpc-api-server/pkg/generated/api/math/v1"
)

var (
	calculator               v1.Server
	float32EqualityThreshold = 1e-9
)

func TestMath(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Math")
}

var _ = BeforeSuite(func() {
	config := zap.NewProductionConfig()
	config.OutputPaths = []string{log.OutputPathStdOut}
	config.ErrorOutputPaths = []string{log.OutputPathStdOut}
	config.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
	config.Encoding = log.FormatConsole
	logger, _ := config.Build()
	calculator = v1.NewServer(&v1.ServerConfig{InputLog: logger, MathService: &fakemath{}})
})

var _ = Describe("Square", func() {
	DescribeTable("simple squares",
		func(ctx context.Context, input, expected float32, expectedError error) {
			result, resultError := calculator.GetSquare(ctx, &pb.GetSquareMsg{Input: input})

			if result == nil {
				Expect(result).ToNot(BeNil())
			} else {
				Expect(result.Result).To(BeNumerically("~", expected, float32EqualityThreshold))
			}
			Expect(status.Code(resultError)).To(Equal(codes.OK))
		},
		Entry("5^2 = 25", nil, float32(5), float32(25), nil),
		Entry("-5^2 = 25", nil, float32(-5), float32(25), nil),
		Entry("0^2 = 0", nil, float32(0), float32(0), nil),
		Entry(".1^2 = .01", nil, float32(.1), float32(0.01), nil),
	)
})

var _ = Describe("SquareRoot", func() {
	DescribeTable("simple squareroots",
		func(ctx context.Context, input, expected float32, expectedError error) {
			result, resultError := calculator.GetSquareRoot(ctx, &pb.GetSquareRootMsg{Input: input})

			if expectedError == nil {
				if result == nil {
					Expect(result).ToNot(BeNil())
				} else {
					Expect(result.Result).To(BeNumerically("~", expected, float32EqualityThreshold))
				}
				Expect(status.Code(resultError)).To(Equal(codes.OK))
			} else {
				Expect(result).To(BeNil())
				Expect(status.Code(resultError)).To(Equal(codes.InvalidArgument))
			}
		},
		Entry("25 = 5", nil, float32(25), float32(5), nil),
		Entry("-5^2 = error", nil, float32(-5), float32(0), status.New(codes.InvalidArgument, "placeholder").Err()),
		Entry("0 = 0", nil, float32(0), float32(0), nil),
		Entry(".01^2 = .1", nil, float32(.01), float32(0.1), nil),
	)
})

var _ = Describe("Factorial", func() {
	DescribeTable("simple factorial",
		func(ctx context.Context, input, expected int64, expectedError error) {
			result, resultError := calculator.GetFactorial(ctx, &pb.GetFactorialMsg{Input: input})

			if expectedError == nil {
				if result == nil {
					Expect(result).ToNot(BeNil())
				} else {
					Expect(result.Result).To(BeNumerically("~", expected, float32EqualityThreshold))
				}
				Expect(status.Code(resultError)).To(Equal(codes.OK))
			} else {
				Expect(result).To(BeNil())
				Expect(status.Code(resultError)).To(Equal(codes.InvalidArgument))
			}
		},
		Entry("5! = 120", nil, int64(5), int64(120), nil),
		Entry("-5! = error", nil, int64(-5), int64(0), status.New(codes.InvalidArgument, "placeholder").Err()),
		Entry("0 = 1", nil, int64(0), int64(1), nil),
		Entry("1 = 1", nil, int64(1), int64(1), nil),
	)
})
