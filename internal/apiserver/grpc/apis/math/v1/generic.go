package v1

import (
	"go.uber.org/zap"

	"gitlab.com/mvenezia/sample-grpc-api-server/internal/math"
)

const (
	ErrInternalServer  = "an error occurred within math api server"
	LoggingPackageName = "internal.apiserver.grpc.apis.math.v1"
)

type Server struct {
	logger      *zap.Logger
	mathService math.Service
}

type ServerConfig struct {
	InputLog    *zap.Logger
	MathService math.Service
}

func NewServer(config *ServerConfig) Server {
	server := Server{
		logger:      config.InputLog.Named(LoggingPackageName),
		mathService: config.MathService,
	}
	server.SetupServices()
	return server
}

func (s *Server) SetupServices() {
	// If not set up, set it up...
	if s.mathService == nil {
		s.mathService = math.NewService()
	}
}

//func wrapInternalServerError(inputError error) error {
//	st := status.New(codes.Internal, ErrInternalServer)
//	st, _ = st.WithDetails(&errdetails.ErrorInfo{
//		Reason: inputError.Error(),
//		Domain: "version",
//	})
//	return st.Err()
//}
