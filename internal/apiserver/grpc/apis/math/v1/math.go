package v1

import (
	"golang.org/x/net/context"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	pb "gitlab.com/mvenezia/sample-grpc-api-server/pkg/generated/api/math/v1"
)

const (
	NegativeNumberProvidedErr = "negative number provided"
)

func (s Server) GetSquare(ctx context.Context, input *pb.GetSquareMsg) (*pb.GetSquareReply, error) {
	square := s.mathService.Square(float64(input.Input))

	return &pb.GetSquareReply{
		Result: float32(square),
	}, nil
}

func (s Server) GetSquareRoot(ctx context.Context, input *pb.GetSquareRootMsg) (*pb.GetSquareRootReply, error) {
	// Checking for dumb inputs...
	if input.Input < 0 {
		// dumb input provided...
		inputStatus := status.New(codes.InvalidArgument, NegativeNumberProvidedErr)
		inputStatus, _ = inputStatus.WithDetails(&errdetails.ErrorInfo{
			Reason: NegativeNumberProvidedErr,
			Domain: "grpc",
		}, &errdetails.BadRequest{
			FieldViolations: []*errdetails.BadRequest_FieldViolation{
				{
					Field:       "input",
					Description: NegativeNumberProvidedErr,
				},
			},
		})
		return nil, inputStatus.Err()
	}

	squareRoot, err := s.mathService.SquareRoot(float64(input.Input))

	if err != nil {
		inputStatus := status.New(codes.Internal, err.Error())
		return nil, inputStatus.Err()
	}

	return &pb.GetSquareRootReply{
		Result: float32(squareRoot),
	}, nil
}

func (s Server) GetFactorial(ctx context.Context, input *pb.GetFactorialMsg) (*pb.GetFactorialReply, error) {
	// Checking for dumb inputs...
	if input.Input < 0 {
		// dumb input provided...
		inputStatus := status.New(codes.InvalidArgument, NegativeNumberProvidedErr)
		inputStatus, _ = inputStatus.WithDetails(&errdetails.ErrorInfo{
			Reason: NegativeNumberProvidedErr,
			Domain: "grpc",
		}, &errdetails.BadRequest{
			FieldViolations: []*errdetails.BadRequest_FieldViolation{
				{
					Field:       "input",
					Description: NegativeNumberProvidedErr,
				},
			},
		})
		return nil, inputStatus.Err()
	}

	factorial, err := s.mathService.Factorial(input.Input)

	if err != nil {
		inputStatus := status.New(codes.Internal, err.Error())
		return nil, inputStatus.Err()
	}

	return &pb.GetFactorialReply{
		Result: factorial,
	}, nil
}
