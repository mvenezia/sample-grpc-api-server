package v1

import (
	"golang.org/x/net/context"

	pb "gitlab.com/mvenezia/sample-grpc-api-server/pkg/generated/api/version/v1"
)

func (s Server) GetInformation(ctx context.Context, input *pb.GetInformationMsg) (*pb.GetInformationReply, error) {
	version := s.versionService.Version(ctx)
	return &pb.GetInformationReply{
		GitVersion:   version.GitVersion,
		GitCommit:    version.GitCommit,
		GitTreeState: version.GitTreeState,
		BuildDate:    version.BuildDate,
		GoVersion:    version.GoVersion,
		Compiler:     version.Compiler,
		Platform:     version.Platform,
	}, nil
}
