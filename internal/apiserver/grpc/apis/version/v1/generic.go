package v1

import (
	promapi "github.com/prometheus/client_golang/api"
	"gitlab.com/mvenezia/sample-grpc-api-server/internal/version"
	"go.uber.org/zap"
)

const (
	ErrInternalServer  = "an error occurred within version api server"
	LoggingPackageName = "internal.apiserver.grpc.apis.version.v1"
)

type Server struct {
	logger *zap.Logger

	versionService version.Service
}

type ServerConfig struct {
	InputLog   *zap.Logger
	PromClient promapi.Client
}

func NewServer(config *ServerConfig) Server {
	server := Server{
		logger: config.InputLog.Named(LoggingPackageName),
	}
	server.SetupServices()
	return server
}

func (s *Server) SetupServices() {
	s.versionService = version.NewService()
}

//func wrapInternalServerError(inputError error) error {
//	st := status.New(codes.Internal, ErrInternalServer)
//	st, _ = st.WithDetails(&errdetails.ErrorInfo{
//		Reason: inputError.Error(),
//		Domain: "version",
//	})
//	return st.Err()
//}
