package v1

import (
	"google.golang.org/grpc"

	pb "gitlab.com/mvenezia/sample-grpc-api-server/pkg/generated/api/version/v1"
)

var (
	APIServer Server
)

type API struct{}

func (api API) RegisterAPI(server *grpc.Server) {
	pb.RegisterVersionServer(server, APIServer)
}
