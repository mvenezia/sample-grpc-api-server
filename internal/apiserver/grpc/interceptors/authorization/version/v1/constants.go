package v1

const (
	// Unfortunately this value is not exported in a useful way from the generated code
	apiPrefix = "/versionv1.VersionAPI/"

	// Strings for errors
	ErrPermissionDenied  = "you are not authorized make this request"
	ErrInternalGRPCError = "an internal-to-ims grpc error happened"

	LoggingPackageName            = "interceptors.authorization.imsapi.v1"
	LogIncomingRequest            = "ims api request made"
	LogIncomingRequestMethodKey   = "method"
	LogValidating                 = "validating authorization for rpc method"
	LogValidatingMethodKey        = LogIncomingRequestMethodKey
	LogEnforcingTrialPeriodFailed = "enforcing the trial period failed"

	RPCGetVersionInformation = "GetVersionInformation"
)
