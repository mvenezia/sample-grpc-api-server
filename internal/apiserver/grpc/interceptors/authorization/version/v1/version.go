package v1

import (
	"context"

	"go.uber.org/zap"
)

// No validation is currently needed...
func validateGetVersionInformation(ctx context.Context, req interface{}) error {
	logger.Debug(LogValidating, zap.String(LogValidatingMethodKey, RPCGetVersionInformation))

	return nil
}
