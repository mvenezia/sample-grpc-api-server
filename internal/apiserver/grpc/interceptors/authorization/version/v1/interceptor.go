package v1

import (
	"context"
	"strings"

	"go.uber.org/zap"
	"google.golang.org/grpc"
)

var (
	logger *zap.Logger
)

func SetupInterceptor(inputLog *zap.Logger) {
	logger = inputLog.Named(LoggingPackageName)
}

func UnaryInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	var err error

	// break out the full method, separate out the method from the api
	// If there is only one string after the split, it means it is not this api
	stringArray := strings.Split(info.FullMethod, apiPrefix)
	if len(stringArray) == 2 {
		// Debugging line
		logger.Info(LogIncomingRequest, zap.String(LogIncomingRequestMethodKey, stringArray[1]))

		// Now we will switch across methods, each having their own ability to decide if the request is legit
		// Context will have the user already
		switch stringArray[1] {
		// Version Methods
		case RPCGetVersionInformation:
			err = validateGetVersionInformation(ctx, req)
		}
		if err != nil {
			return nil, err
		}
	}

	// Calls the handler as we're ok with the request
	return handler(ctx, req)
}

//func getUsername(ctx context.Context) string {
//	// Grab the username
//	username := metautils.ExtractIncoming(ctx).Get(constants.AuthorizedUserHeader)
//
//	// Everything seems to check out, return the username
//	return username
//}
//
//func wrapPermissionDeniedError(inputError error) error {
//	if inputError == nil {
//		inputError = fmt.Errorf(ErrPermissionDenied)
//	}
//	st := status.New(codes.PermissionDenied, ErrPermissionDenied)
//	st, err := st.WithDetails(&errdetails.ErrorInfo{
//		Reason: inputError.Error(),
//		Domain: "authorization",
//	})
//	if err != nil {
//		log.Printf("could not attach %s to a permission denied error!", inputError.Error())
//	}
//	return st.Err()
//}
//
//func createGenericInternalAuthorizationError() error {
//	// We somehow are internally broken...
//	st := status.New(codes.Internal, ErrInternalGRPCError)
//	st, _ = st.WithDetails(&errdetails.ErrorInfo{
//		Reason: "could determine if user has access to resource",
//		Domain: "grpc",
//	})
//	return st.Err()
//}
