package prometheus

import (
	interceptor "github.com/grpc-ecosystem/go-grpc-prometheus"
	"google.golang.org/grpc"

	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/prometheus"
)

var (
	grpcMetrics = interceptor.NewServerMetrics()
)

func UnaryFunction() grpc.UnaryServerInterceptor {
	return grpcMetrics.UnaryServerInterceptor()
}

func StreamFunction() grpc.StreamServerInterceptor {
	return grpcMetrics.StreamServerInterceptor()
}

func RegisterGRPCServer(server *grpc.Server) {
	prometheus.GetRegistry().MustRegister(grpcMetrics)
	//grpcMetrics.EnableHandlingTimeHistogram()
	grpcMetrics.InitializeMetrics(server)
}
