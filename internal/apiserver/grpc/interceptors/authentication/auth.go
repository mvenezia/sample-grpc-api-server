package authentication

import (
	"context"
	"strings"

	"github.com/coreos/go-oidc"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/mvenezia/sample-grpc-api-server/internal/apiserver/grpc/constants"
)

const (
	CannotVerifyCredentials = "cannot verify credentials"

	ErrCouldNotEstablishTokenValidator = "could not reach oidc endpoint to create a token validator"

	authenticationResult = "authentication result"
	loggerModuleName     = "interceptors.authentication"
)

var (
	oidcAudience string
	oidcURL      string

	tokenValidator *oidc.IDTokenVerifier

	logger *zap.Logger
)

type Error struct {
	Message string
	Err     error
}

func (e *Error) Error() string { return e.Message + ": " + e.Err.Error() }

func SetupAuthentication(parentLog *zap.Logger, URL string, audience string) {
	oidcURL = URL
	oidcAudience = audience
	logger = parentLog.Named(loggerModuleName)
}

func UnaryAuthenticationInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	var err error

	// If we're being used, for security, we should reset the user and groups information
	resetUserAndGroups(ctx)

	// Let's challenge that token
	err = challengeToken(ctx)
	if err != nil {
		return nil, err
	}

	logger.Debug(
		authenticationResult,
		zap.String("user", metautils.ExtractIncoming(ctx).Get(constants.AuthorizedUserHeader)),
		zap.String("groups", metautils.ExtractIncoming(ctx).Get(constants.AuthorizedGroupsHeader)))
	h, err := handler(ctx, req)
	return h, err
}

func challengeToken(ctx context.Context) error {
	authorizationArray := strings.Split(metautils.ExtractIncoming(ctx).Get(constants.AuthorizationHeader), " ")
	if len(authorizationArray) != 2 {
		//return status.Error(codes.Unauthenticated, "not really allowed")
		return nil
	}
	token := authorizationArray[1]

	user, groups, err := verifyOIDCToken(ctx, token)
	if err != nil {
		logger.Debug(authenticationResult, zap.Error(err))
		return status.Error(codes.Unauthenticated, CannotVerifyCredentials)
	}

	setUserAndGroups(ctx, user, groups)
	return nil
}

func resetUserAndGroups(ctx context.Context) {
	metautils.ExtractIncoming(ctx).Set(constants.AuthorizedUserHeader, "")
	metautils.ExtractIncoming(ctx).Set(constants.AuthorizedGroupsHeader, "")
}

func setUserAndGroups(ctx context.Context, user string, groups []string) {
	metautils.ExtractIncoming(ctx).Set(constants.AuthorizedUserHeader, user)
	metautils.ExtractIncoming(ctx).Set(constants.AuthorizedGroupsHeader, strings.Join(groups, ","))
}
