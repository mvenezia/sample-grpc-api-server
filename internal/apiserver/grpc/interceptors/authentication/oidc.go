package authentication

import (
	"context"

	"github.com/coreos/go-oidc"
	"go.uber.org/zap"
)

func getOIDCProvider(c context.Context) (*oidc.Provider, error) {
	return oidc.NewProvider(c, oidcURL)
}

func getOIDCIDTokenVerifier(provider *oidc.Provider) *oidc.IDTokenVerifier {
	return provider.Verifier(&oidc.Config{
		ClientID: oidcAudience,
		// Enable this to disable audience check...
		//SkipClientIDCheck: true,
		SkipExpiryCheck: false,
	})
}

func setupTokenVerifier() error {
	provider, err := getOIDCProvider(context.Background())
	if err != nil {
		return err
	}

	tokenValidator = getOIDCIDTokenVerifier(provider)
	return nil
}

func verifyOIDCToken(c context.Context, rawIDToken string) (user string, groups []string, err error) {
	if tokenValidator == nil {
		err = setupTokenVerifier()
		if err != nil {
			logger.Warn(ErrCouldNotEstablishTokenValidator, zap.Error(err))
			err = &Error{
				Message: ErrCouldNotEstablishTokenValidator,
				Err:     err,
			}
			return
		}
	}

	// Parse and verify ID Token payload.
	idToken, err := tokenValidator.Verify(c, rawIDToken)
	if err != nil {
		return
	}

	// Extract custom claims.
	var verifyInfo oidcClaims
	if err = idToken.Claims(&verifyInfo); err != nil {
		return
	}

	user = verifyInfo.Email
	groups = verifyInfo.Groups
	return
}

type oidcClaims struct {
	Issuer          string   `json:"iss"`
	Subject         string   `json:"sub"`
	Audience        string   `json:"aud"`
	Expires         int64    `json:"exp"`
	IssuedAt        int64    `json:"iat"`
	AccessTokenHash string   `json:"at_hash"`
	Email           string   `json:"email"`
	Verified        bool     `json:"email_verified"`
	Groups          []string `json:"groups"`
	Name            string   `json:"name"`
	IDToken         string   `json:"id_token"`
}
