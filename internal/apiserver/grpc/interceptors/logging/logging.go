package logging

import (
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	"go.uber.org/zap"
	"google.golang.org/grpc"

	"gitlab.com/mvenezia/sample-grpc-api-server/internal/log"
)

const (
	loggerModuleName = "internal.apiserver.grpc.interceptors.logging"
)

var (
	logger *zap.Logger
	opts   = []grpc_zap.Option{}
)

// SetupLogging
// Will create a logger for this component
// Also will replace whatever logger grpc is using with the new logger
func SetupLogging() {
	logger = log.GetLogger().Named(loggerModuleName)
	grpc_zap.ReplaceGrpcLoggerV2(logger)
}

func UnaryFunction() grpc.UnaryServerInterceptor {
	return grpc_zap.UnaryServerInterceptor(logger, opts...)
}

func StreamFunction() grpc.StreamServerInterceptor {
	return grpc_zap.StreamServerInterceptor(logger, opts...)
}
