package apiserver

const (
	CannotSetupLoggingError           = "could not set up logging"
	CannotCreatePrometheusClientError = "could not create prometheus client"
)

type Error struct {
	Message string
	Err     error
}

func (e *Error) Error() string { return e.Message + ": " + e.Err.Error() }
