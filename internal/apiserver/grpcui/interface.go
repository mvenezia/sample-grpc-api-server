package grpcui

import (
	"context"

	"go.uber.org/zap"
	"google.golang.org/grpc"
)

const (
	LoggingPackageName = "internal.apiserver.grpcui"
)

type Server interface {
	Serve()
}

type Error struct {
	Message string
	Err     error
}

func (e *Error) Error() string { return e.Message + ": " + e.Err.Error() }

type ServerConfiguration struct {
	GRPCConnectPort string
	WebListenPort   string

	Context           context.Context
	ErrorChannel      chan<- error
	GRPCDialerOptions []grpc.DialOption
	Logger            *zap.Logger
}
