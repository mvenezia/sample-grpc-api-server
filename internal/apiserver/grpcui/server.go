package grpcui

import (
	"context"
	"net/http"

	"github.com/fullstorydev/grpcui/standalone"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

const (
	connectAddressPrefix = "dns:///localhost:"
	listenAddressPrefix  = ":"

	ErrCannotCreateClientConnection = "cannot create grpc client connection"
	ErrCannotCreateHandler          = "cannot create grpcui handler"
	ErrServerError                  = "error serving up grpcui"

	LogStartingServerListAddress = "listenAddress"
	LogStartingServerMessage     = "starting server"
)

type server struct {
	grpcConnectPort string
	webListenPort   string

	context           context.Context
	errorChannel      chan<- error
	grpcDialerOptions []grpc.DialOption
	logger            *zap.Logger
}

func New(inputConfig ServerConfiguration) Server {
	return &server{
		context:           inputConfig.Context,
		errorChannel:      inputConfig.ErrorChannel,
		grpcConnectPort:   inputConfig.GRPCConnectPort,
		grpcDialerOptions: inputConfig.GRPCDialerOptions,
		webListenPort:     inputConfig.WebListenPort,
		logger:            inputConfig.Logger,
	}
}

func (s *server) Serve() {
	// Create the client connection
	clientConnection, err := grpc.DialContext(s.context, connectAddressPrefix+s.grpcConnectPort, s.grpcDialerOptions...)
	if err != nil {
		s.logger.Error(ErrCannotCreateClientConnection, zap.Error(err))
		s.errorChannel <- &Error{Message: ErrCannotCreateClientConnection, Err: err}
		return
	}

	// Generate the handler
	handler, err := standalone.HandlerViaReflection(s.context, clientConnection, connectAddressPrefix+s.grpcConnectPort)
	if err != nil {
		s.logger.Error(ErrCannotCreateHandler, zap.Error(err))
		s.errorChannel <- &Error{Message: ErrCannotCreateHandler, Err: err}
		return
	}

	// Start serving http traffic...
	s.logger.Info(LogStartingServerMessage, zap.String(LogStartingServerListAddress, s.webListenPort))
	err = http.ListenAndServe(listenAddressPrefix+s.webListenPort, handler)
	if err != nil {
		s.logger.Error(ErrServerError, zap.Error(err))
		s.errorChannel <- &Error{Message: ErrServerError, Err: err}
		return
	}
}
