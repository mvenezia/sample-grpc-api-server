package configuration

type Server struct {
	Authentication Authentication
	Logging        Logging
	Services       Services
	Metrics        Metrics
}

type GRPC struct {
	Port         string
	Interceptors GRPCInterceptors
}

type GRPCUI struct {
	Port string
}

type Prometheus struct {
	Port string
}

type Rest struct {
	Port string
}

type Services struct {
	GRPC       GRPC
	GRPCUI     GRPCUI
	Prometheus Prometheus
	Rest       Rest
}

type GRPCInterceptors struct {
	AuthenticationEnabled bool
	AuthorizationEnabled  bool
	PrometheusEnabled     bool
	RecoveryEnabled       bool
}

type Authentication struct {
	OIDCAudience string
	OIDCURL      string
}

type Logging struct {
	Format          string
	Level           int
	OutputPath      string
	ErrorOutputPath string
}

type Metrics struct {
	Enabled bool
}
