package prometheus

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
)

type server struct {
	webListenPort string

	errorChannel chan<- error
	logger       *zap.Logger
}

func New(inputConfig ServerConfiguration) Server {
	return &server{
		errorChannel:  inputConfig.ErrorChannel,
		webListenPort: inputConfig.WebListenPort,
		logger:        inputConfig.Logger,
	}
}

func (s *server) Serve() {
	// Create a HTTP server for prometheus.
	httpServer := &http.Server{
		Handler: promhttp.HandlerFor(registry, promhttp.HandlerOpts{}),
		Addr:    listenAddressPrefix + s.webListenPort,
	}

	// Starting webserver
	s.logger.Info(LogStartingServerMessage, zap.String(LogStartingServerListAddress, s.webListenPort))
	err := httpServer.ListenAndServe()
	if err != nil {
		s.logger.Error(ErrServerError, zap.Error(err))
		s.errorChannel <- &Error{Message: ErrServerError, Err: err}
		return
	}
}
