package prometheus

import (
	"go.uber.org/zap"
)

const (
	LoggingPackageName = "internal.apiserver.prometheus"

	listenAddressPrefix = "0.0.0.0:"

	ErrServerError = "error serving up grpcui"

	LogStartingServerListAddress = "listenAddress"
	LogStartingServerMessage     = "starting server"
)

type Server interface {
	Serve()
}

type Error struct {
	Message string
	Err     error
}

func (e *Error) Error() string { return e.Message + ": " + e.Err.Error() }

type ServerConfiguration struct {
	WebListenPort string

	ErrorChannel chan<- error
	Logger       *zap.Logger
}
