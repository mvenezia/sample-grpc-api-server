package prometheus

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	// Create a metrics registry.
	registry = prometheus.NewRegistry()
)

func GetRegistry() *prometheus.Registry {
	return registry
}
