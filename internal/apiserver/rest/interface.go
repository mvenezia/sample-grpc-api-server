package rest

import (
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

const (
	LoggingPackageName = "internal.apiserver.rest"
)

type Server interface {
	Serve()
}

type ServerConfiguration struct {
	ErrorChannel chan<- error

	GRPCListenAddress string
	RESTListenAddress string

	GRPCDialerOptions []grpc.DialOption
	Logger            *zap.Logger
}
