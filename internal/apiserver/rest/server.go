package rest

import (
	"context"
	"mime"
	"net/http"
	"strings"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	assetfs "github.com/philips/go-bindata-assetfs"
	"go.uber.org/zap"
	"google.golang.org/grpc"

	mathv1pb "gitlab.com/mvenezia/sample-grpc-api-server/pkg/generated/api/math/v1"
	versionv1pb "gitlab.com/mvenezia/sample-grpc-api-server/pkg/generated/api/version/v1"
	"gitlab.com/mvenezia/sample-grpc-api-server/pkg/generated/ui/data/homepage"
	"gitlab.com/mvenezia/sample-grpc-api-server/pkg/generated/ui/data/protobuf"
	"gitlab.com/mvenezia/sample-grpc-api-server/pkg/generated/ui/data/swagger"
	"gitlab.com/mvenezia/sample-grpc-api-server/pkg/generated/ui/data/swaggerjson"
	"gitlab.com/mvenezia/sample-grpc-api-server/pkg/marshaller"
)

const (
	APIGatewayPath = "/api/"

	StaticHomepagePath    = "/"
	StaticProtobufPath    = "/protobuf/"
	StaticSwaggerPath     = "/swagger/"
	StaticSwaggerUIPath   = "/swagger-ui/"
	StaticSwaggerUIPrefix = "swagger-ui"

	AccessControlAllowHeaders    = "Access-Control-Allow-Headers"
	AccessControlAllowMethods    = "Access-Control-Allow-Methods"
	AccessControlAllowOrigin     = "Access-Control-Allow-Origin"
	AccessControlRequestMethod   = "Access-Control-Request-Method"
	HTTPMethodOptions            = "OPTIONS"
	HTTPHeaderContentType        = "Content-Type"
	JSONContentType              = "application/json"
	JSONFileExtension            = ".json"
	OriginHeaderKey              = "Origin"
	PreFlightHeaderAccept        = "Accept"
	PreFlightHeaderAuthorization = "Authorization"
	PreFlightHeaderContentType   = HTTPHeaderContentType
	PreFlightMethodDelete        = "DELETE"
	PreFlightMethodGet           = "GET"
	PreFlightMethodHead          = "HEAD"
	PreFlightMethodPost          = "POST"
	PreFlightMethodPut           = "PUT"
	SVGContentType               = "image/svg+xml"
	SVGFileExtension             = ".svg"
	WebhookContentType           = "application/raw-webhook"
	WebhookPath                  = "webhook"

	LogPreFlightNoticeMessage      = "preflight request for"
	LogPreFlightNoticeURL          = "url"
	LogRegisteringGRPCAddress      = "grpcAddress"
	LogRegisteringGRPCMessage      = "registering grpc server"
	LogRegisteringHomepageHandler  = "registering homepage handler (/)"
	LogRegisteringProtobufHandler  = "registering protobuf handler (/protobuf/)"
	LogRegisteringSwaggerHandler   = "registering protobuf handler (/swagger/)"
	LogRegisteringSwaggerUIHandler = "registering protobuf handler (/swagger-ui/)"
	LogStartingServerListAddress   = "listenAddress"
	LogStartingServerMessage       = "starting server"
)

var (
	PreFlightHeaders = []string{PreFlightHeaderAccept, PreFlightHeaderAuthorization, PreFlightHeaderContentType}
	PreFlightMethods = []string{PreFlightMethodDelete, PreFlightMethodGet, PreFlightMethodHead, PreFlightMethodPost, PreFlightMethodPut}
)

type server struct {
	errorChannel chan<- error

	grpcListenAddress string
	restListenAddress string

	mux            *http.ServeMux
	restGatewayMux *runtime.ServeMux

	grpcDialerOptions []grpc.DialOption
	logger            *zap.Logger
}

func New(inputConfig ServerConfiguration) Server {
	return &server{
		errorChannel:      inputConfig.ErrorChannel,
		grpcListenAddress: inputConfig.GRPCListenAddress,
		grpcDialerOptions: inputConfig.GRPCDialerOptions,
		restListenAddress: inputConfig.RESTListenAddress,
		logger:            inputConfig.Logger,
	}
}

func (s *server) Serve() {
	// Start the main HTTP Mux
	s.mux = http.NewServeMux()

	// Add the static content (swagger, homepage, etc)
	s.addStaticContent()

	// Create the HTTP Rest Gateway for the gRPC service
	s.createRestGatewayMux()

	// Tell the mainHTTPMux to send API traffic (most likely /api/ - check constant) to the restGatewayMux
	s.mux.Handle(APIGatewayPath, s.restGatewayMux)

	// Starting the http listener with the mainHTTPMux
	s.logger.Info(LogStartingServerMessage, zap.String(LogStartingServerListAddress, s.restListenAddress))
	s.errorChannel <- http.ListenAndServe(s.restListenAddress, s.customMimeWrapper(s.allowCORS(s.mux)))

}

func (s *server) createRestGatewayMux() {
	jsonpb := &runtime.JSONPb{}
	s.restGatewayMux = runtime.NewServeMux(
		runtime.WithMarshalerOption(WebhookContentType, &marshaller.RawJSONPb{JSONPb: jsonpb}),
		runtime.WithMarshalerOption(runtime.MIMEWildcard, jsonpb),
		runtime.WithErrorHandler(runtime.DefaultHTTPErrorHandler))

	s.logger.Debug(LogRegisteringGRPCMessage, zap.String(LogRegisteringGRPCAddress, s.grpcListenAddress))
	err := versionv1pb.RegisterVersionHandlerFromEndpoint(context.Background(), s.restGatewayMux, s.grpcListenAddress, s.grpcDialerOptions)
	if err != nil {
		s.errorChannel <- err
	}
	err = mathv1pb.RegisterMathHandlerFromEndpoint(context.Background(), s.restGatewayMux, s.grpcListenAddress, s.grpcDialerOptions)
	if err != nil {
		s.errorChannel <- err
	}
}

func (s *server) allowCORS(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if origin := r.Header.Get(OriginHeaderKey); origin != "" {
			w.Header().Set(AccessControlAllowOrigin, origin)
			if r.Method == HTTPMethodOptions && r.Header.Get(AccessControlRequestMethod) != "" {
				s.preflightHandler(w, r)
				return
			}
		}
		h.ServeHTTP(w, r)
	})
}

// Allow the admission webhook rest endpoint to pass json data as raw string
// so that we don't have to write out entirety of SubjectAccessReview in our
// proto definition
func (s *server) customMimeWrapper(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Contains(r.URL.Path, WebhookPath) {
			r.Header.Set(HTTPHeaderContentType, WebhookContentType)
		}
		h.ServeHTTP(w, r)
	})
}

func (s *server) preflightHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(AccessControlAllowHeaders, strings.Join(PreFlightHeaders, ","))
	w.Header().Set(AccessControlAllowMethods, strings.Join(PreFlightMethods, ","))
	s.logger.Info(LogPreFlightNoticeMessage, zap.String(LogPreFlightNoticeURL, r.URL.Path))
}

func (s *server) addStaticContent() {
	s.serveSwagger()
	s.serveSwaggerJSON()
	s.serveProtoBuf()
	s.serveHomepage()
}

func (s *server) serveSwagger() {
	// Ignoring the error returned. Should this be logged?
	_ = mime.AddExtensionType(SVGFileExtension, SVGContentType)

	// Expose files in third_party/swagger-ui/ on <host>/swagger-ui
	fileServer := http.FileServer(&assetfs.AssetFS{
		Asset:    swagger.Asset,
		AssetDir: swagger.AssetDir,
		Prefix:   StaticSwaggerUIPrefix,
	})
	s.logger.Debug(LogRegisteringSwaggerUIHandler)
	s.mux.Handle(StaticSwaggerUIPath, http.StripPrefix(StaticSwaggerUIPath, fileServer))
}

func (s *server) serveSwaggerJSON() {
	// Ignoring the error returned. Should this be logged?
	_ = mime.AddExtensionType(JSONFileExtension, JSONContentType)

	fileServer := http.FileServer(&assetfs.AssetFS{
		Asset:    swaggerjson.Asset,
		AssetDir: swaggerjson.AssetDir,
	})
	s.logger.Debug(LogRegisteringSwaggerHandler)
	s.mux.Handle(StaticSwaggerPath, http.StripPrefix(StaticSwaggerPath, fileServer))
}

func (s *server) serveProtoBuf() {

	fileServer := http.FileServer(&assetfs.AssetFS{
		Asset:    protobuf.Asset,
		AssetDir: protobuf.AssetDir,
	})
	s.logger.Debug(LogRegisteringProtobufHandler)
	s.mux.Handle(StaticProtobufPath, http.StripPrefix(StaticProtobufPath, fileServer))
}

func (s *server) serveHomepage() {

	fileServer := http.FileServer(&assetfs.AssetFS{
		Asset:    homepage.Asset,
		AssetDir: homepage.AssetDir,
	})
	s.logger.Debug(LogRegisteringHomepageHandler)
	s.mux.Handle(StaticHomepagePath, http.StripPrefix(StaticHomepagePath, fileServer))
}
