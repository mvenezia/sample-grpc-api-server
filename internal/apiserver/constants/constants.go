package constants

import "go.uber.org/zap/zapcore"

const (
	GRPCPort           = "grpc-port"
	RESTPort           = "rest-port"
	PrometheusPort     = "prometheus-port"
	GRPCUIPort         = "grpcui-port"
	OIDCURL            = "oidc-url"
	OIDCAudience       = "oidc-audience"
	GRPCAuthentication = "grpc-authentication"
	GRPCAuthorization  = "grpc-authorization"
	GRPCPrometheus     = "grpc-prometheus"
	GRPCRecovery       = "grpc-recovery"
	LogLevel           = "log-level"
	LogOutput          = "log-output"
	LogErrorOutput     = "log-output-stderror"
	LogFormat          = "log-format"
	MetricsEnabled     = "metrics-enabled"

	GRPCPortDefault           = 9051
	RESTPortDefault           = 9052
	PrometheusPortDefault     = 9053
	GRPCUIPortDefault         = 9054
	OIDCURLDefault            = "https://dex.maxwell.quacks.org"
	OIDCAudienceDefault       = "sample-grpc-api-server"
	GRPCAuthenticationDefault = true
	GRPCAuthorizationDefault  = true
	GRPCPrometheusDefault     = true
	GRPCRecoveryDefault       = true
	LogLevelDefault           = int(zapcore.WarnLevel)
	LogOutputDefault          = "stdout"
	LogErrorOutputDefault     = "stdout"
	LogFormatDefault          = "json"
	MetricsEnabledDefault     = true

	GRPCPortDescription           = "gRPC listen port"
	RESTPortDescription           = "Web/REST/Swagger port"
	PrometheusPortDescription     = "Prometheus exporter port"
	GRPCUIPortDescription         = "gRPC-UI port"
	OIDCURLDescription            = "OIDC provider URL"
	OIDCAudienceDescription       = "Audience to verify against when using OIDC Authentication"
	GRPCAuthenticationDescription = "Enable OIDC/JWT based authentication in gRPC server"
	GRPCAuthorizationDescription  = "Enable authorization in gRPC server"
	GRPCPrometheusDescription     = "Enable prometheus exporting in gRPC server"
	GRPCRecoveryDescription       = "Have gRPC server recover from panics with gRPC error"
	LogLevelDescription           = "What level of logging should we support, \n\t-1: Debug, \n\t 0: Info, \n\t 1: Warning \n\t 2: Error \n\t 3: Debug Panic, \n\t 4: Panic \n\t 5: Fatal\nSet to a number"
	LogOutputDescription          = "Where should non-error log messages be sent, stderr and stdout currently supported"
	LogErrorOutputDescription     = "Where should error log messages be sent, stderr and stdout currently supported"
	LogFormatDescription          = "Format of log output, currently only json and console are supported"
	MetricsEnabledDescription     = "Enable reporting metrics"

	ProjectPrefix = "SGAS"
)
