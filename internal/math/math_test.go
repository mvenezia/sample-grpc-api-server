package math_test

import (
	"fmt"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/ginkgo/extensions/table"
	. "github.com/onsi/gomega"

	"gitlab.com/mvenezia/sample-grpc-api-server/internal/math"
)

func TestMath(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Math")
}

var _ = Describe("Square", func() {
	var calculator math.Service
	var float64EqualityThreshold = 1e-9

	BeforeEach(func() {
		calculator = math.NewService()
	})

	DescribeTable("simple squares",
		func(x, expected float64) {
			Expect(calculator.Square(x)).To(BeNumerically("~", expected, float64EqualityThreshold))
		},
		Entry("5^2 = 25", float64(5), float64(25)),
		Entry("-5^2 = 25", float64(-5), float64(25)),
		Entry("0^2 = 0", float64(0), float64(0)),
		Entry(".1^2 = .01", float64(.1), float64(0.01)),
	)
})

var _ = Describe("SquareRoot", func() {
	var calculator math.Service
	var float64EqualityThreshold = 1e-9

	BeforeEach(func() {
		calculator = math.NewService()
	})

	DescribeTable("simple squareroots",
		func(x, expected float64, expectedError error) {
			result, resultError := calculator.SquareRoot(x)
			Expect(result).To(BeNumerically("~", expected, float64EqualityThreshold))
			if expectedError != nil {
				if mathError, ok := resultError.(*math.Error); ok {
					Expect(mathError.Message).To(Equal(math.SquareRootNegativeNumberError))
				} else {
					Expect(fmt.Sprintf("%T", resultError)).To(Equal(fmt.Sprintf("%T", &math.Error{})))
				}
			}
		},
		Entry("25 = 5", float64(25), float64(5), nil),
		Entry("-5^2 = error", float64(-5), float64(0), &math.Error{Message: math.SquareRootNegativeNumberError}),
		Entry("0 = 0", float64(0), float64(0), nil),
		Entry(".01^2 = .1", float64(.01), float64(0.1), nil),
	)
})

var _ = Describe("Factorial", func() {
	var calculator math.Service

	BeforeEach(func() {
		calculator = math.NewService()
	})

	DescribeTable("simple factorials",
		func(x, expected int64, expectedError error) {
			result, resultError := calculator.Factorial(x)
			Expect(result).To(BeNumerically("==", expected))
			if expectedError != nil {
				if mathError, ok := resultError.(*math.Error); ok {
					Expect(mathError.Message).To(Equal(math.FactorialNegativeNumberError))
				} else {
					Expect(fmt.Sprintf("%T", resultError)).To(Equal(fmt.Sprintf("%T", &math.Error{})))
				}
			}
		},
		Entry("5! = 120", int64(5), int64(120), nil),
		Entry("-5! = error", int64(-5), int64(0), &math.Error{Message: math.FactorialNegativeNumberError}),
		Entry("0 = 1", int64(0), int64(1), nil),
		Entry("1 = 1", int64(1), int64(1), nil),
	)
})
