package math

import (
	"math"
)

type Service interface {
	Square(input float64) float64
	SquareRoot(input float64) (float64, error)
	Factorial(input int64) (int64, error)
}

type service struct{}

func NewService() Service {
	return service{}
}

func (service) Square(input float64) float64 {
	return input * input
}

func (service) SquareRoot(input float64) (float64, error) {
	if input < 0 {
		return 0, &Error{Message: SquareRootNegativeNumberError}
	}
	return math.Sqrt(input), nil
}

func (service) Factorial(input int64) (int64, error) {
	if input < 0 {
		return 0, &Error{Message: FactorialNegativeNumberError}
	}
	if input < 2 {
		return 1, nil
	}
	output := input
	for x := input - 1; x > 1; x-- {
		output = output * x
	}
	return output, nil
}
