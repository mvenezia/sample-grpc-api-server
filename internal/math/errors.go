package math

const (
	SquareRootNegativeNumberError = "cannot square root a negative number"
	FactorialNegativeNumberError  = "cannot factorial a negative number"
)

type Error struct {
	Message string
	Err     error
}

func (e *Error) Error() string {
	if e.Err != nil {
		return e.Message + ": " + e.Err.Error()
	} else {
		return e.Message
	}
}
