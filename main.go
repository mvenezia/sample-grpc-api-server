package main

import (
	"gitlab.com/mvenezia/sample-grpc-api-server/cmd/sample-grpc-api-server/cmd"
)

func main() {
	cmd.Execute()
}
