#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

THIS_DIRECTORY=$(dirname "$BASH_SOURCE")
PROJECT_DIRECTORY=$THIS_DIRECTORY/../..

echo "Ensuring directories exist"
mkdir -p ${PROJECT_DIRECTORY}/pkg/generated/ui/data/swagger \
         ${PROJECT_DIRECTORY}/pkg/generated/ui/data/protobuf \
         ${PROJECT_DIRECTORY}/pkg/generated/ui/data/swaggerjson \
         ${PROJECT_DIRECTORY}/pkg/generated/ui/data/homepage \
         ${PROJECT_DIRECTORY}/assets/generated/swagger

DOCKER_IMAGE="quay.io/venezia/go-bindata-assetfs:v1.0.1"
DOCKER_ARGUMENTS="docker run --rm=true -it -v $PWD/${PROJECT_DIRECTORY}:/code"

echo "generating swagger-ui bindata file..."
$DOCKER_ARGUMENTS $DOCKER_IMAGE -pkg swagger -o /code/pkg/generated/ui/data/swagger/bindata.go -prefix /code/third_party /code/third_party/swagger-ui

echo "generating protobuf files..."
$DOCKER_ARGUMENTS $DOCKER_IMAGE  -pkg protobuf -o /code/pkg/generated/ui/data/protobuf/bindata.go -prefix /code/api/sample-grpc-api-spec/api /code/api/sample-grpc-api-spec/api/...

echo "generating swagger.json files..."
$DOCKER_ARGUMENTS $DOCKER_IMAGE  -pkg swaggerjson -o /code/pkg/generated/ui/data/swaggerjson/bindata.go -prefix /code/assets/generated/swagger /code/assets/generated/swagger/...

echo "generating homepage files..."
$DOCKER_ARGUMENTS $DOCKER_IMAGE  -pkg homepage -o /code/pkg/generated/ui/data/homepage/bindata.go -prefix /code/assets/homepage /code/assets/homepage
