#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

THIS_DIRECTORY=$(dirname "${BASH_SOURCE}")
PROJECT_DIRECTORY=${THIS_DIRECTORY}/../..
GO_IMAGE=quay.io/venezia/golang:1.17.0

echo
echo "running unit tests ..."
echo "docker run --rm -v ${PWD}/${PROJECT_DIRECTORY}:/app -w /app ${GO_IMAGE} ./build/scripts/unit-test-container.sh"
docker run --rm -v ${PWD}/${PROJECT_DIRECTORY}:/app -w /app ${GO_IMAGE} ./build/scripts/unit-test-container.sh
